%define parse.error verbose
%define parse.trace

%code requires {
	#include <stdio.h>
	#include <stdlib.h>
	
	extern void yyerror(const char*);
	extern FILE *yyin;
}

%code {
	extern int yylex();
	extern int yylineno;
}

%union {
	char *string;
	double floatValue;
	int intValue;
}

%token AND           "&&"
%token OR            "||"
%token EQ            "=="
%token NEQ           "!="
%token LEQ           "<="
%token GEQ           ">="
%token LSS           "<"
%token GRT           ">"
%token KW_BOOLEAN    "bool"
%token KW_DO         "do"
%token KW_ELSE       "else"
%token KW_FLOAT      "float"
%token KW_FOR        "for"
%token KW_IF         "if"
%token KW_INT        "int"
%token KW_PRINTF     "printf"
%token KW_RETURN     "return"
%token KW_VOID       "void"
%token KW_WHILE      "while"
%token CONST_INT     "integer literal"
%token CONST_FLOAT   "float literal"
%token CONST_BOOLEAN "boolean literal"
%token CONST_STRING  "string literal"
%token ID            "identifier"
%nonassoc "then"
%nonassoc "else"
%left EQ NEQ LEQ GEQ LSS GRT
%left '+' '-' OR
%left '*' '/' AND
%nonassoc UMINUS

%%

program: declassignment ';' program |functiondefinition program | %empty;
functiondefinition: type id '(' parameterlist2 ')' '{' statementlist '}';
parameterlist2 : parameterlist | %empty;
parameterlist : type id moreparameters;
moreparameters : ',' type id moreparameters | %empty;
functioncall : id '(' functioncall2 ')';
functioncall2 : %empty | assignment moreassignments;
moreassignments : ',' assignment moreassignments | %empty;
statementlist : block statementlist | %empty;
block : '{' statementlist '}' | statement;
statement : ifstatement | forstatement | whilestatement | returnstatement ';' | dowhilestatement ';'| printf ';'| declassignment ';' | statassignment ';' | functioncall ';';
statblock : '{' statementlist '}' | statement;
ifstatement : KW_IF '(' assignment ')' statblock %prec "then" | KW_IF '(' assignment ')' statblock KW_ELSE statblock;
forstatement : KW_FOR '(' statassignment ';' expr ';' statassignment ')' statblock | KW_FOR '(' declassignment ';' expr ';' statassignment ')' statblock;
dowhilestatement : KW_DO statblock KW_WHILE '(' assignment ')';
whilestatement : KW_WHILE '(' assignment ')' statblock;
returnstatement : KW_RETURN assignment | KW_RETURN;
printf : KW_PRINTF '(' CONST_STRING ')' | KW_PRINTF '(' assignment ')';
declassignment : type id | type id '=' assignment;
statassignment : id '=' assignment;
assignment : id '=' assignment | expr;
expr : simpexpr | simpexpr comparison;
comparison : "==" simpexpr | "!=" simpexpr | "<=" simpexpr | ">=" simpexpr | "<" simpexpr | ">" simpexpr;
simpexpr : '-' term halfexpr %prec UMINUS| term halfexpr;
halfexpr : %empty | '+' term halfexpr | '-' term halfexpr %prec UMINUS| "||" term halfexpr;
term : factor morefactors;
morefactors : %empty | '*' factor morefactors| '/' factor morefactors| "&&" factor morefactors;
factor : CONST_INT | CONST_FLOAT | CONST_BOOLEAN | functioncall | id | '(' assignment ')';
type : KW_BOOLEAN | KW_FLOAT| KW_INT| KW_VOID;
id : ID;


%%

int main(int argc, char *argv[]) {
	yydebug = 0;
	if (argc >= 1){
		yyin = fopen(argv[1], "r");
    }
	if(yyin == NULL){
	  printf("Die Datei kann nicht geoeffnet werden.\n");
	  exit(-1);
    }
	return yyparse();
}

void yyerror(const char *msg) {
	printf("Parsefehler\n");
	exit(-1);
}
